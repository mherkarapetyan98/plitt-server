const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const messageSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    maxlength: 50
  },
  email: {
    type: String,
    required: true
  },
  review: {
    type: String,
    maxlength: 1200
  },
  date: Date,
  createdDate: String
});

module.exports = mongoose.model("Reviews", messageSchema, "Reviews");
