const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const messageSchema = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 16
    },
    phone: {
        type: Number,
        required: true,
        minlength: 3,
        maxlength: 16
    },
    message: {
        type: String, 
        maxlength: 200
    },
    date: String
}); 

module.exports = mongoose.model('messages', messageSchema, 'messages');