'use strict'; 

const mongoose = require('mongoose');
const Schema = mongoose.Schema;  
const mongoosePaginate = require('mongoose-paginate');
const productSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    category: {
        ref: 'categories',
        type: Schema.Types.ObjectId,
        required: true
    },
    imageSrc: {
        type: String,
        default: ''
    },
    size: {
        type: String,
    },
    height: {
        type: String,
        required: false
    },
    quantity: {
        type: String,
        required: false
    },
    process: {
        type: String,
        required: false
    },
    price: {
        type: Number,
    },
    priceColor: {
        type: Number
    },
    imageName: String

})

productSchema.plugin(mongoosePaginate)

module.exports = mongoose.model('Products', productSchema,   'Products')