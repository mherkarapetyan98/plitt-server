'use strict';

const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const passport = require('passport');
const Admin = require('../models/Admin');
const keys = require('../config/keys');

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: keys.jwt
};

passport.use(
    new JwtStrategy(options, async (payload, done) => {
        try {
            const user = await Admin.findOne({ username: payload.username }).select('username');
            if (user) {
                done(null, user);
            } else {
                done(null, false);
            }
        } catch (err) {
            console.log(err);
        }
    })
);

module.exports = passport.authenticate('jwt', { session: false });
