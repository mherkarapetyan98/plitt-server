'use strict';

const express = require('express');  
const router = express.Router(); 

const productController = require('../controllers/Product');


// router.get('/:categoryKey', productController.getProductByCategory); 
router.get('/:categoryId', productController.getProductByCategory);   

router.get('/:categoryKey/:productId', productController.getProduct);   

module.exports = router;