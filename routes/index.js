'use strict';

const express = require('express');
const router = express.Router();

const adminRouter = require('./admin.js');
const categoryRouter = require('./category.js');
const productRouter = require('./product.js');
const messagesRouter = require('./messages');
const reviewsRouter = require('./reviews');


router.use('/admin', adminRouter);
router.use('/category', categoryRouter);
router.use('/product', productRouter);  
router.use('/reviews', reviewsRouter);
router.use('/messages', messagesRouter);

module.exports = router;
