'use strict';

const express = require('express');  
const router = express.Router(); 

const MessagesController = require('../controllers/MessagesController');


router.post('/', MessagesController.create);
router.delete('/:id', MessagesController.removeMessage);
router.get('/', MessagesController.getAll);

module.exports = router;