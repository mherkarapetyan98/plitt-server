'use strict';

const express = require('express');  
const router = express.Router(); 

const ReviewsController = require('../controllers/ReviewsController');


router.post('/', ReviewsController.create);
router.delete('/:id', ReviewsController.removeReview);
router.get('/', ReviewsController.getAll);
router.get('/admin', ReviewsController.getAllDashboard);
router.get('/:reviewId', ReviewsController.getReview);   
router.patch('/:id', ReviewsController.actionUpdate); 



module.exports = router;  