'use strict';

const express = require('express');  
const router = express.Router(); 
const auth = require('../middleware/passport');

const upload = require('../middleware/upload'); 
const AdminController = require('../controllers/AdminController');
 
 
router.delete('/:id', AdminController.actionRemove); 
router.post('/', upload.single('image'), AdminController.actionCreate); 
router.post('/login', AdminController.actionLogin); 
router.post('/is-logged-in', auth, AdminController.actionIsLoggedIn);
router.get('/:categoryKey', AdminController.getAll);   



module.exports = router; 