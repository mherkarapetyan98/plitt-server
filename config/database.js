'use strict';

const express = require('express');

const mongoose = require('mongoose');
const keys = require('./keys')
mongoose.set('useFindAndModify', false);

//  'mongodb://127.0.0.1:27017/plitt' don't touch mongodb://127.0.0.1:27017/plitt
//'mongodb://admin:123456a@ds061839.mlab.com:61839/plitt'
//  never touch !!! mongodb://127.0.0.1:27017/plitt

mongoose.connect('mongodb://127.0.0.1:27017/plitt', { useNewUrlParser: true })
    .then(() => console.log('MongoDB successfully connected'))
    .catch(error => console.error('Mongo connection error:', error));
module.exports = mongoose; 