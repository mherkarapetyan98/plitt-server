"use strict";

const Review = require("../models/Reviews");
const moment = require("moment");

class ReviewsController {
  static async create(req, res) {
    // let today = moment().format("YYYY-MM-DD");
    let today = moment().format("DD/MM/YY");

    const review = new Review({
      name: req.body.name,
      email: req.body.email,
      review: req.body.review,
      date: req.body.date,
      createdDate: req.body.date
    });
    try {
      await review.save();
      res.status(201).json(review);
    } catch (e) {
      res.status();
    }
  }

  static async getReview(req, res) {
    try {   
        const review = await Review.findOne({
            _id: req.params.reviewId
        })
        res.status(200).json(review) 
    } catch (error) {
        console.log(e)
    }
}


  static async getAll(req, res) {
    try {
      const page = parseInt(req.query.page) || 1;
        const size = req.query.size || 6;
      const countDoc = await Review.find(); 
      const reviews = await Review.find() 
        .sort({ 'date': -1 }) 
        .skip(size * (page - 1))
        .limit(size)
        .exec((count_error, count) => { 
          res.status(200).json({
            pages: Math.ceil(countDoc.length  / size),
            page: page,
            next: page + 1,
            prev: page - 1,
            reviews: count
          });
        });
    } catch (e) {
      console.log(e);
    }
  }

  static async getAllDashboard(req, res) {
    try {
        const reviews = await Review.find();
        res.status(200).json(reviews.reverse())            
    } catch (e) {
        console.log(e)
    }     
}

  static async removeReview(req, res) {
    try {
      await Review.deleteOne({ _id: req.params.id });
      const review = await Review.find();
      res.status(200).json(review.reverse());
    } catch (e) {
      console.log(e);
    }
  }


  static async actionUpdate(req, res) {
    // let today = moment().format("YYYY-MM-DD");
    let today = moment().format("DD/MM/YY");


    const review = await Review.findOne({ _id: req.params.id });
    const updated = {    
      name: req.body.name ? req.body.name : review.name,
      email: req.body.email ? req.body.email : review.email,
      review: req.body.review ? req.body.review : review.review,
      date: req.body.date ? req.body.date : review.date,
      createdDate: req.body.date ? req.body.date : review.createdDate
    };
 
    try { 
      const category = await Review.findOneAndUpdate(
        { _id: req.params.id },
        { $set: updated },
        { new: true }
      );
      res.status(200).json(category);
    } catch (e) {
      console.log(e)
    }
  }

}

module.exports = ReviewsController;
