'use strict';
 
const Message = require('../models/Message')

class MessagesController { 
    static async create(req, res) {
        const message = new Message({
            name:  req.body.name,
            phone: req.body.phone,
            message: req.body.message,
            date: req.body.date
        }) 
        try {
            await message.save();
            res.status(201).json(message);
        } catch (e) {
            res.status()
        }
    }

    static async getAll(req, res) {
        try {
            const messages = await Message.find();
            res.status(200).json(messages.reverse())            
        } catch (e) {
            console.log(e)
        }     
    }
    
    static async removeMessage(req,res) {
        try {
           await Message.deleteOne({_id : req.params.id})
           const messages = await Message.find()
            res.status(200).json(messages.reverse());
        } catch (e) {
            console.log(e)
        }
    }
}


module.exports = MessagesController;