'use strict';

 
const Category = require('../models/Category');

class CategoryController {
    /* Get Trotuar product  */
    static async getCategory(req, res) {     
        try {
            const categories = await Category.find();
            res.status(200).json(categories)            
        } catch (e) {
            console.log(e)
        }  
    }
 
}


module.exports = CategoryController;