"use strict";

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const Admin = require("../models/Admin");
const keys = require("../config/keys");
const errorHandler = require("../utils/errorHandler");
const Product = require("../models/Product"); 
const Category = require('../models/Category');

class AdminController {
  static async actionLogin(req, res) {     
    try {
    const candidate = await Admin.findOne({
      username: req.body.username
    });
    const passwordResult =   await bcrypt.compare(  
      req.body.password,
      candidate.password
      );
      if (candidate) {
        if (passwordResult) {
  
          const token = jwt.sign(
            {
              username: candidate.username
            },
            keys.jwt,
            {
              expiresIn: "30d"
            }
          );
          res.status(200).json({
            token: `Bearer ${token}`
          });
        } else {
           res.status(401).json({
            message: "password not correct"
          })
        }
      } else {
         res.status(404).json({
          message: "not admin"
        });
      }
    } catch (error) {
      res.status(500).json({
        success: false,
        message: error.message ? error.message : error
    })
    }
  }

   
  /* is logged in */
  static actionIsLoggedIn(req, res) {
    return res.send(true);
  }

  /* GetAll product*/

  static async getAll(req, res) {
   
  try { 
    const products = await Product
        .find()
        .populate({
            path: 'category',
            match: {
                key: req.params.categoryKey
            }
        })
        .exec();
    res.status(200).json(products.filter(product => product.category))
} catch (e) {
    console.log(e)
}
  }

  /* Create product  */
  static async actionCreate(req, res) {
    const product = new Product({
      name: req.body.name,
      category: [req.body.category],
      imageSrc: req.file ? req.file.path : "",
      size: req.body.size,
      height: req.body.height,
      quantity: req.body.quantity,
      process: req.body.process,
      price: req.body.price,
      priceColor: req.body.priceColor,
      imageName: req.body.imageName
    });
    try {
      await product.save();
      res.status(201).json(product);
    } catch (e) {
      console.log(e);
    }
  }

  /* Remove product  */
  static async actionRemove(req, res) {
    try {
      await Product.deleteOne({ _id: req.params.id });
      res.status(200).json({
        message: "Category was deleted"
      });
    } catch (e) {
      console.log(e);
    }
  }

  

  /* Update product  */
  static async actionUpdate(req, res) {
    const product = await Product.findOne({ _id: req.params.id });
    const updated = {
      name: req.body.name ? req.body.name : product.name,
      category: req.body.category ? req.body.category : product.category,
      size: req.body.size ? req.body.size : product.size,
      height: req.body.height ? req.body.height : product.height,
      quantity: req.body.quantity ? req.body.quantity : product.quantity,
      process: req.body.process ? req.body.process : product.process,
      price: req.body.price ? req.body.price : product.price,
      priceColor: req.body.priceColor ? req.body.priceColor : product.priceColor 
    };

    if (req.file) { 
      updated.imageSrc = req.file.path 
      updated.imageName = req.file.filename || product.imageName
    }

    try { 
      const category = await Product.findOneAndUpdate(
        { _id: req.params.id },
        { $set: updated },
        { new: true }
      );
      res.status(200).json(category);
    } catch (e) {
      errorHandler(res, e);
    }
  }
}

module.exports = AdminController;
