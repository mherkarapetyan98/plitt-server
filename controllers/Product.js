'use strict';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const Admin = require('../models/Admin');
const keys = require('../config/keys');

const Product = require('../models/Product');
const Category = require('../models/Category');

class ProductController {
    static async getProductByCategory(req, res) {
        try {
            const page = parseInt(req.query.page) || 1;
            const size = req.query.size || 7
            const query = {}            
            let filteredProduct = [];
            const products = await Product.find({ category: req.params.categoryId })
            .populate("category")
            .skip(size * (page - 1))
            .limit(size)
            .exec((err, product) => {
              Product.countDocuments({ category: req.params.categoryId })
                .populate("category")
                .exec((count_error, count) => {
                  res.status(200).json({
                    pages: Math.ceil(count / size),
                    page: page,
                    next: page + 1,
                    prev: page - 1,
                    products: product
                  });
                });
            });
        } catch (e) {
            console.log(e)
        }
    }

    static async getProduct(req, res) {
        try { 
            const product = await Product.findOne({
                _id: req.params.productId
            })
            res.status(200).json(product)
            console.log(product)
        } catch (error) {
            console.log(e)
        }
    }

    static async getAll(req, res) {
        try {
            console.log(req.params.categoryKey)
            const products = await Product
                .find()
                .populate({
                    path: 'category',
                    match: {
                        key: req.params.categoryKey
                    }
                })
                .exec();
            res.status(200).json(products.filter(product => product.category))
        } catch (e) {
            console.log(e)
        }
    }
}


module.exports = ProductController;