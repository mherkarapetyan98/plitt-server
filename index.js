'use strict';

const port = process.env.PORT || 5000
const cors = require('cors');
const express = require('express');
const passport = require('passport');
const bodyParser = require('body-parser');
const app = express();
const connection = require('./config/database')
const api = require('./routes/index');

app.use(passport.initialize());

app.use(cors({
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}));
  
app.use(bodyParser.json());
app.use(require('cors')());
app.use('/uploads', express.static('uploads'))
// app.use('/static', express.static('../plitt-client/src/img/products/'));
// app.use(express.static(__dirname, 'public'));
app.use('/api', api);

app.listen(port, console.log(`Listening to ${port}`));